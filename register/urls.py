from django.urls import path
from . import views

urlpatterns = [
    path('', views.home, name='register-home'),
    path('about/',views.about, name='register-about'),
]